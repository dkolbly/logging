package logging

import (
	"encoding/json"
	"io"
	"os"
)

type Writer interface {
	Write(map[string]interface{}) error
}

var globalOutput Writer = &jsonOutput{os.Stdout}

func SetGlobalOutput(w Writer) {
	globalOutput = w
}

func GlobalOutput() Writer {
	return globalOutput
}

type multi struct {
	first Writer
	rest  Writer
}

func (m multi) Write(fields map[string]interface{}) error {
	m.first.Write(fields)
	return m.rest.Write(fields)
}

func (m multi) Unwrap() Writer {
	return m.rest // chain "deeper"
}

func AddGlobalOutput(w Writer) {
	globalOutput = multi{w, globalOutput}
}

type jsonOutput struct {
	dest io.Writer
}

func (j *jsonOutput) Write(fields map[string]interface{}) error {
	b, err := json.Marshal(fields)
	if err != nil {
		return err
	}
	_, err = j.dest.Write(append(b, '\n'))
	return err
}

type null struct {
}

// Null is a Writer that discards everything
var Null = null{}

func (n null) Write(_ map[string]interface{}) error {
	return nil
}
