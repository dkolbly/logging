package main

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strconv"

	"github.com/agnivade/levenshtein"
)

var re = regexp.MustCompile(`"([A-Z]{3}-[0-9]{4})([ :,]([^"]*))?"`)
var catalogEntry = regexp.MustCompile(`^([A-Z]{3}-[0-9]{4}) ([a-zA-Z0-9/.-]+):([0-9]+)(: (".*"))
`)

func main() {

	top := findClosestGoModule()
	catalog := loadCatalog(filepath.Join(top, "message.cat"))

	fn := &fileNumbers{
		index:     make(map[string][]occurrence),
		committed: catalog,
	}

	for _, arg := range os.Args[1:] {
		fn.scanFile(arg)
	}

	dups := 0
	for k, lst := range fn.index {
		if len(lst) > 1 {
			if dups == 0 {
				fmt.Fprintf(os.Stderr, "Duplicate Message IDs:\n")
			}
			dups++
			for i, occur := range lst {
				if i == 0 {
					fmt.Fprintf(os.Stderr, "%s ", k)
				} else {
					fmt.Fprintf(os.Stderr, "         ")
				}
				if occur.text != "" {
					fmt.Fprintf(os.Stderr, "%s:%d: %q\n",
						occur.file, occur.line, occur.text)
				} else {
					fmt.Fprintf(os.Stderr, "%s:%d\n",
						occur.file, occur.line)
				}
			}
			// if this id was previously registered, we can try to guess
			// which is the "real" one.
			if cataloged, ok := fn.committed[k]; ok {
				closest := -1
				closestDistance := 1000000

				for i, occur := range lst {
					distance, info := cataloged[0].distance(occur)
					if closest == -1 || distance < closestDistance {
						closest = i
						closestDistance = distance
					}
					fmt.Fprintf(os.Stderr, "  [%d]  %s:%d: <> catalog:  d=%d  (%s)\n",
						i,
						occur.file, occur.line,
						distance, info)
				}
				fmt.Fprintf(os.Stderr, "best guess is that [%d] is the real one and renum the others\n",
					closest)
			}
		}
	}
	// don't do reconciliation if there are problems
	// just within the input dataset?
	if dups > 0 {
		os.Exit(1)
	}

	// reconcile the committed list with the stuff we just scanned
	for k, v := range fn.index {
		if lst, ok := fn.committed[k]; ok {
			reconcile(k, lst[0], v[0])
		} else {
			fmt.Fprintf(os.Stderr, "%s is new\n", k)
		}
	}
}

func reconcile(key string, was, now occurrence) {
	if was == now {
		fmt.Fprintf(os.Stderr, "%s has not changed at all\n", key)
		return
	}
	filechange := was.file != now.file
	linechange := filechange || (was.line != now.line)
	textchange := was.text != now.text
	fmt.Fprintf(os.Stderr, "%s changed: file=%t line=%t text=%t\n", key,
		filechange,
		linechange,
		textchange,
	)
}

type occurrence struct {
	file string
	line int
	text string
}

type fileNumbers struct {
	index     map[string][]occurrence
	committed map[string][]occurrence
}

func (fn *fileNumbers) scanFile(file string) {
	fd, err := os.Open(file)
	if err != nil {
		panic(err)
	}
	src := bufio.NewReader(fd)
	linenum := 0
	for {
		line, err := src.ReadBytes('\n')
		if err != nil {
			break
		}
		//fmt.Printf("--- %s", line)
		linenum++
		m := re.FindSubmatch(line)
		if m != nil {
			mid := string(m[1])
			text := string(m[3])
			if len(text) > 0 {
				fmt.Printf("%s %s:%d: %q\n",
					mid,
					file,
					linenum,
					text)
			} else {
				fmt.Printf("%s %s:%d\n",
					mid,
					file,
					linenum)
			}
			o := occurrence{
				file: file,
				line: linenum,
				text: text,
			}
			fn.index[mid] = append(fn.index[mid], o)
		}
	}
}

func findClosestGoModule() string {

	here, err := filepath.Abs(".")
	if err != nil {
		panic(err)
	}

	for {
		_, err := os.Stat(filepath.Join(here, "go.mod"))
		if err == nil {
			return here
		}
		if here == "/" {
			return "."
		}
		here = filepath.Join(here, "..")
	}
}

func loadCatalog(srcfile string) map[string][]occurrence {
	cat := make(map[string][]occurrence)

	fd, err := os.Open(srcfile)
	if err != nil {
		return cat
	}

	defer fd.Close()

	src := bufio.NewReader(fd)
	for {
		line, err := src.ReadBytes('\n')
		if err != nil {
			break
		}
		m := catalogEntry.FindSubmatch(line)
		if m != nil {
			key := string(m[1])
			file := string(m[2])
			line, _ := strconv.Atoi(string(m[3]))
			text, e := strconv.Unquote(string(m[5]))
			if e != nil {
				panic(e)
			}
			fmt.Printf("LD %s %s:%d: %q\n", key, file, line, text)
			o := occurrence{
				file: file,
				line: line,
				text: text,
			}
			cat[key] = append(cat[key], o)
		}
	}

	return cat
}

func iabs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

func (o occurrence) distance(to occurrence) (int, string) {
	td := levenshtein.ComputeDistance(o.text, to.text)

	if o.file != to.file {
		return 200 + td, fmt.Sprintf("different files, text diff=%d", td)
	}
	ld := iabs(o.line - to.line)
	return ld + td, fmt.Sprintf("line diff=%d text diff=%d", ld, td)
}
