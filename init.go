package logging

import (
	"fmt"
	"io"
	"os"
)

type Config struct {
	// Output sets the output
	Output io.Writer
	// ForceJSON makes the output be JSON even if the output is to
	// a terminal
	ForceJSON bool
	// ForcePretty turns on pretty output, regardless of whether
	// output is a terminal
	ForcePretty bool
	// DisableColor turns off colorization in pretty mode even if
	// the output is a terminal
	DisableColor bool
	// ForceColor turns on colorization even if the output is not
	// a terminal
	ForceColor bool
	// DisableIndent turns off indenting of multi-line messages,
	// i.e., so that
	//
	//   log.Debugf(ctx, "Foo\nbar")
	//
	// renders as:
	//
	//   14:19:27.800 DEBUG    [module|file.go:20] Foo
	//   bar
	//
	// instead of this, which is the default:
	//
	//   14:19:27.800 DEBUG    [module|file.go:20] Foo
	//                                             bar
	//
	DisableIndent bool
	// DisableExtraFields turns off output of extra fields in pretty mode
	// (by default, extra fields are output)
	DisableExtraFields bool
	// Priority sets the logging level; only messages with this level
	// of severity (priority) and higher will be output
	Priority Priority
	// Filters is a list of output middleware filters; they are
	// applied in-order, meaning that the first filter in this list
	// is called with out outbound message first, then the one after that
	// etc.
	Filters []Filter
}

type Option func(*Config)

func Init(opts ...Option) {
	cfg := &Config{
		Priority: PriorityInfo,
	}
	for _, opt := range opts {
		opt(cfg)
	}

	var output io.Writer = os.Stdout

	if cfg.Output != nil {
		output = cfg.Output
	}

	var isTTY, ttyChecked bool

	tty := func() bool {
		if !ttyChecked {
			ttyChecked = true
			if file, ok := output.(*os.File); ok {
				isTTY = isTerminal(file)
			}
		}
		return isTTY
	}

	pretty := false

	switch {
	case cfg.ForceJSON:
		pretty = false
	case cfg.ForcePretty:
		pretty = true
	default:
		pretty = tty()
	}

	var dest Writer
	if pretty {
		color := false

		switch {
		case cfg.DisableColor:
			color = false
		case cfg.ForceColor:
			color = true
		default:
			color = tty()
		}

		po := makePrettyOutput(!cfg.DisableIndent, color, output)

		if !cfg.DisableExtraFields {
			po.showExtra = true
		}
		dest = po
	} else {
		dest = &jsonOutput{output}
	}

	// push the filters in reverse order, so the last one
	// transforms the message last
	for i := range cfg.Filters {
		filter := cfg.Filters[len(cfg.Filters)-i-1]
		dest = filter.Apply(dest)
	}

	// push the priority filter on
	// last, so that it happens first
	if cfg.Priority > PriorityTrace {
		filter := PriorityFilter(cfg.Priority)
		dest = filter.Apply(dest)
	}

	globalOutput = dest
}

func ForceJSON(c *Config) {
	c.ForceJSON = true
}

func Output(dst io.Writer) Option {
	return func(c *Config) {
		c.Output = dst
	}
}

// Quiet produces an option that, if the argument
// is true, sets the logging level to WARNING
func Quiet(flag bool) Option {
	if !flag {
		return func(*Config) {}
	}
	return func(c *Config) {
		c.Priority = PriorityWarning
	}
}

// Debug produces an option that, if the argument is true,
// sets the logging level to DEBUG
func Debug(flag bool) Option {
	if !flag {
		return func(*Config) {}
	}
	return func(c *Config) {
		c.Priority = PriorityDebug
	}
}

func LowestPriority(p Priority) Option {
	return func(c *Config) {
		c.Priority = p
	}
}

// this is intended to be handed "unsafe" input, e.g., from an
// environment variable like LOG_LEVEL, so it will output a warning to
// stderr.  As an exception to that rule, an empty string is silently
// accepted as meaning "don't set the logging level", which in the
// absence of other options means that INFO will be used
func LowestLevel(s string) Option {
	o := Option(func(*Config) {})

	if s != "" {
		if p, ok := PriorityFromString(s); ok {
			o = LowestPriority(p)
		} else {
			fmt.Fprintf(os.Stderr, "ERROR LowestLevel(%q) is not a valid logging level\n", s)
		}
	}
	return o
}
