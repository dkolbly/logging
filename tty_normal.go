//go:build linux || darwin || windows
// +build linux darwin windows

package logging

import (
	"os"

	"github.com/mattn/go-isatty"
)

func isTerminal(f *os.File) bool {
	return isatty.IsTerminal(f.Fd())
}
