package rotation

import (
	"encoding/json"
	"os"
	"strings"
	"time"
)

// RotatingFileOutput writes line-oriented JSON to an output file,
// closing and shifting to a new file after certain amount of time or
// size.  Files are named with timestamps and the output is specified
// using a glob.
type Output struct {
	dest          string
	destGlobIndex int
	tsFormat      string
	maxTime       time.Duration
	maxSize       uint64
	ch            chan map[string]interface{}
	push          bool
}

type Option func(c *Output) error

func MustNew(opts ...Option) *Output {
	out, err := New(opts...)
	if err != nil {
		panic(err)
	}
	return out
}

func New(opts ...Option) (*Output, error) {
	c := &Output{
		dest:     "/tmp/output.*.json",
		tsFormat: "2006-01-02-150405",
		maxTime:  24 * time.Hour,
		maxSize:  10 << 20,
		ch:       make(chan map[string]interface{}, 100),
	}
	for _, opt := range opts {
		err := opt(c)
		if err != nil {
			return nil, err
		}
	}
	c.destGlobIndex = strings.LastIndexByte(c.dest, '*')

	first, err := c.next()
	if err != nil {
		return nil, err
	}
	go c.feed(first)
	return c, nil
}

func FilePattern(glob string) Option {
	return func(c *Output) error {
		c.dest = glob
		return nil
	}
}

func (out *Output) Write(fields map[string]interface{}) error {
	f2 := make(map[string]interface{}, len(fields)+1)
	for k, v := range fields {
		f2[k] = v
	}
	out.ch <- f2
	return nil
}

func tagContinue(ch byte) bool {
	return (ch >= 'a' && ch <= 'z') || (ch >= '0' && ch <= '9')
}

func (out *Output) serz(seq uint64, fields map[string]interface{}) []byte {
	if v, ok := fields["@timestamp"]; ok {
		if t, ok := v.(time.Time); ok {
			fields["@timestamp"] = t.UTC().Format("2006-01-02T15:04:05.000Z")
			fields["nsec"] = t.Nanosecond()
		}
	}
	fields["seq"] = seq

	var module string
	if m, ok := fields["module"]; ok {
		if s, ok := m.(string); ok {
			module = s
		}
	}

	allowTags := true
	var tags []string
	if tf, ok := fields["tags"]; ok {
		if lst, ok := tf.([]string); ok {
			tags = make([]string, len(lst)+1)
			copy(tags, lst)
		} else {
			allowTags = false
		}
	}

	if allowTags {
		if m, ok := fields["message"]; ok {
			if text, ok := m.(string); ok {
				if len(text) > 0 && text[0] == '#' {
					i := 1
					for i < len(text) && tagContinue(text[i]) {
						i++
					}
					tag := text[1:i]
					if module != "" {
						tag = module + "." + tag
					}
					tags = append(tags, tag)
					text = text[i+1:]
					fields["message"] = text
					fields["tags"] = tags
				}
			}
		}
	}

	b, err := json.Marshal(fields)
	if err != nil {
		return nil
	}
	return append(b, '\n')
}

func (out *Output) feed(current *os.File) {
	started := time.Now()
	var written uint64
	var count uint64

	lost := 0

	overflows := func(n int) bool {
		return written+uint64(n) > out.maxSize || time.Since(started) > out.maxTime
	}

	for rec := range out.ch {
		count++
		line := out.serz(count, rec)
		if line == nil {
			lost++
			continue
		}

		if current != nil && overflows(len(line)) {
			out.flush(current)
			current = nil
		}
		if current == nil {
			var err error
			current, err = out.next()
			if err != nil {
				lost++
				continue
			}
			started = time.Now()
			written = 0
		}

		n, err := current.Write(line)
		written += uint64(n)
		if err != nil {
			out.flush(current)
			current = nil
			lost++
			continue
		}
	}
}

func (out *Output) next() (*os.File, error) {
	f := out.dest
	k := out.destGlobIndex
	if k >= 0 {
		tf := time.Now().Format(out.tsFormat)
		f = f[:k] + tf + f[k+1:]
	}
	return os.Create(f)
}

func (out *Output) flush(f *os.File) {
	f.Close()
}
