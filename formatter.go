package logging

import (
	"bytes"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type outputContext struct {
	dst        bytes.Buffer
	src        map[string]interface{}
	leftMargin int
	column     int
	bol        bool
	color      bool
}

func (ctx *outputContext) Message() string {
	if m, ok := ctx.src["message"]; ok {
		if s, ok := m.(string); ok {
			return s
		}
	}
	return ""
}

func (ctx *outputContext) Module() string {
	if m, ok := ctx.src["module"]; ok {
		if s, ok := m.(string); ok {
			return s
		}
	}
	return ""
}

func priority(src map[string]interface{}) Priority {
	value, ok := src["level"]
	if !ok {
		return PriorityInfo
	}
	str, ok := value.(string)
	if !ok {
		return PriorityInfo
	}
	if p, ok := levelToPriority[Level(str)]; ok {
		return p
	}
	if p, ok := levelToPriority[Level(strings.ToLower(str))]; ok {
		return p
	}
	return PriorityInfo
}

func level(src map[string]interface{}) Level {
	return priorityToLevel[priority(src)]
}

func (ctx *outputContext) Level() Level {
	return level(ctx.src)
}

func (ctx *outputContext) Write(data []byte) (int, error) {
	for _, b := range data {
		if b == '\n' {
			ctx.dst.WriteByte(b)
			ctx.bol = true
		} else {
			if ctx.bol {
				for j := 0; j < ctx.leftMargin; j++ {
					ctx.dst.WriteByte(' ')
				}
				ctx.column = ctx.leftMargin
				ctx.bol = false
			}
			ctx.dst.WriteByte(b)
			ctx.column++
		}
	}
	return len(data), nil
}

type fragmentFormatter func(*outputContext)

// TODO we can unfold the Write() loop by pre-scanning the literal data
// and splitting it into lines (and special case the 1-line case, and maybe
// even the 1 byte case)
func literals(lit []byte) fragmentFormatter {
	return func(ctx *outputContext) {
		ctx.Write(lit)
	}
}

func literal(lit string) fragmentFormatter {
	return func(ctx *outputContext) {
		ctx.Write([]byte(lit))
	}
}

var formatRe *regexp.Regexp = regexp.MustCompile(`%{([a-z/]+)(?::(.*?[^\\]))?}`)

func compilePattern(pat string) ([]fragmentFormatter, []fragmentFormatter, []string, error) {
	// Find all the %{...} pieces
	matches := formatRe.FindAllStringSubmatchIndex(pat, -1)
	if matches == nil {
		return nil, nil, nil, fmt.Errorf("logger: invalid log format: %q", pat)
	}

	var frags []fragmentFormatter
	var nocolorFrags []fragmentFormatter

	push := func(ff fragmentFormatter, iscolor bool) {
		frags = append(frags, ff)
		if !iscolor {
			nocolorFrags = append(nocolorFrags, ff)
		}
	}

	prev := 0
	require := []string{}
	for _, m := range matches {
		start, end := m[0], m[1]
		if start > prev {
			push(literal(pat[prev:start]), false)
		}
		verb := pat[m[2]:m[3]]
		layout := ""
		if m[4] != -1 {
			layout = pat[m[4]:m[5]]
		}
		var frag fragmentFormatter
		var err error

		fragMaker, ok := verbTable[verb]
		if !ok {
			return nil, nil, nil, fmt.Errorf("logger: unknown verb %q in %q", verb, pat)
		}
		frag, err = fragMaker(layout)
		if err != nil {
			return nil, nil, nil, err
		}
		push(frag, colorTable[verb])
		prev = end
	}
	if prev < len(pat) {
		push(literal(pat[prev:]), false)
	}
	return frags, nocolorFrags, require, nil
}

type fragMaker func(string) (fragmentFormatter, error)

var colorTable = map[string]bool{
	"color":  true,
	"/color": true,
}

var verbTable = map[string]fragMaker{
	"time":       makeTimeFrag,
	"message":    makeMessageFrag,
	"msgid":      makeMsgIDFrag,
	"color":      makeColorFrag,
	"/color":     makeColorResetFrag,
	"module":     makeModuleFrag,
	"source":     makeSourceFrag,
	"level":      makeLevelFrag,
	"leftmargin": makeLeftMarginFrag,
}

func stringopt(options string) string {
	if options == "" {
		return "%s"
	} else {
		return "%" + options
	}
}

func makeLeftMarginFrag(_ string) (fragmentFormatter, error) {
	return func(ctx *outputContext) {
		ctx.leftMargin = ctx.column
	}, nil
}

func makeModuleFrag(options string) (fragmentFormatter, error) {
	options = stringopt(options)
	return func(ctx *outputContext) {
		fmt.Fprintf(ctx, options, ctx.Module())
	}, nil
}

type Sourcer interface {
	File() string
	Line() int
}

func makeSourceFrag(options string) (fragmentFormatter, error) {
	if options == "" {
		options = "%[1]s:%[2]d"
	}
	return func(ctx *outputContext) {
		src := ctx.src["loc"]
		if src, ok := src.(string); ok {
			k := strings.LastIndexByte(src, ':')
			if k < 0 {
				return
			}
			file := src[:k]
			line, _ := strconv.Atoi(src[k+1:])
			fmt.Fprintf(ctx, options, file, line)
		}
	}, nil
}

func makeLevelFrag(options string) (fragmentFormatter, error) {
	options = stringopt(options)
	return func(ctx *outputContext) {
		fmt.Fprintf(ctx, options, strings.ToUpper(string(ctx.Level())))
	}, nil
}

const rfc3339Milli = "2006-01-02T15:04:05.999Z07:00"

func makeTimeFrag(options string) (fragmentFormatter, error) {
	if options == "" {
		options = rfc3339Milli
	}
	return func(ctx *outputContext) {
		ts := ctx.src["@timestamp"]
		if ts, ok := ts.(time.Time); ok {
			ctx.Write([]byte(ts.Format(options)))
		}
	}, nil

}

func makeMessageFrag(options string) (fragmentFormatter, error) {
	if options == "" {
		options = "%s"
	} else {
		options = "%" + options
	}
	return func(ctx *outputContext) {
		fmt.Fprintf(ctx, options, ctx.Message())
	}, nil
}
