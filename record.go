package logging

import (
	"time"
)

type Record struct {
	Timestamp time.Time `json:"@timestamp"`
	Level     string    `json:"level"`
	Loc       string    `json:"loc"`
	LocFile   string    `json:"loc_file"`
	Module    string    `json:"module"`
	Message   string    `json:"message"`
}
