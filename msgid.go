package logging

import (
	"fmt"
	"regexp"
)

var MessageIDPattern = regexp.MustCompile(`^([A-Z]{3}-[0-9]{3,5}) `)

type MessageIDer interface {
	MessageID() string
}

func Split(msg string) (string, string) {
	parts := MessageIDPattern.FindStringSubmatch(msg)
	if parts == nil {
		return "", msg
	}
	return parts[1], msg[len(parts[0]):]
}

func makeMsgIDFrag(options string) (fragmentFormatter, error) {
	return func(ctx *outputContext) {
		if id := msgid(ctx.src); id != "" {
			if ctx.color {
				// bypass the ctx.Write() layer
				// because we don't want this to count
				// against our columns
				ctx.dst.Write(msgidblue)
			}
			ctx.Write([]byte(id))
			if ctx.color {
				// bypass the ctx.Write() layer
				// because we don't want this to count
				// against our columns
				ctx.dst.Write(colorReset)
			}
			ctx.Write([]byte{' '})
		}
	}, nil

}

func msgid(m map[string]interface{}) string {
	id, ok := m["msgid"]
	if !ok {
		return ""
	}
	str, ok := id.(string)
	if !ok {
		str = fmt.Sprintf("%v", id)
	}
	return str
}
