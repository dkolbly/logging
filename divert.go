package logging

import (
	"context"
	"sync"
)

// Tee inserts a tee into the logging context.  Returns a context
// which, when supplied to subsequent logging functions, will cause
// the `capture` argument to also receive the logging event.
func Tee(old context.Context, capture Writer) context.Context {
	return set(old, keyTee, capture)
}

type Diversion struct {
	lock    sync.Mutex
	Entries []map[string]interface{}
}

func (d *Diversion) Write(fields map[string]interface{}) error {
	d.lock.Lock()
	d.Entries = append(d.Entries, fields)
	d.lock.Unlock()
	return nil
}

func WithDiversion(ctx context.Context) (context.Context, *Diversion) {
	d := &Diversion{}
	return Tee(ctx, d), d
}

func WithOutputTo(ctx context.Context, w Writer) context.Context {
	return set(ctx, keyOutput, w)
}
