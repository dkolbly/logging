package logging

import (
	"strings"
)

type Level string

const (
	AUDIT    = Level("audit")
	CRITICAL = Level("critical")
	ERROR    = Level("error")
	WARNING  = Level("warning")
	INFO     = Level("info")
	DEBUG    = Level("debug")
	TRACE    = Level("trace")
)

type Priority int

const (
	PriorityTrace = Priority(iota + 1)
	PriorityDebug
	PriorityInfo
	PriorityWarning
	PriorityError
	PriorityCritical
	PriorityAudit
)

const maxPriority = PriorityCritical

var priorityToLevel = []Level{
	PriorityCritical: CRITICAL,
	PriorityError:    ERROR,
	PriorityWarning:  WARNING,
	PriorityInfo:     INFO,
	PriorityDebug:    DEBUG,
	PriorityTrace:    TRACE,
	PriorityAudit:    AUDIT,
}

var levelToPriority = map[Level]Priority{
	CRITICAL: PriorityCritical,
	ERROR:    PriorityError,
	WARNING:  PriorityWarning,
	INFO:     PriorityInfo,
	DEBUG:    PriorityDebug,
	AUDIT:    PriorityAudit,
	TRACE:    PriorityTrace,
}

func (l Level) Less(b Level) bool {
	return levelToPriority[l] < levelToPriority[b]
}

func PriorityFromString(s string) (Priority, bool) {
	value, ok := levelToPriority[Level(strings.ToLower(s))]
	return value, ok
}

type filteredWriter struct {
	next  Writer
	allow [PriorityAudit]bool
	p     Priority
}

func (f *filteredWriter) Unwrap() Writer {
	return f.next
}

func (f *filteredWriter) Write(record map[string]interface{}) error {
	if f.allow[priority(record)-1] {
		return f.next.Write(record)
	}
	return nil
}

// LevelFilter produces a filter that allows only the given
// level and above messages
func LevelFilter(l Level) Filter {
	if p, ok := levelToPriority[l]; ok {
		return PriorityFilter(p)
	}
	// rather than crash, just default to INFO priority
	return PriorityFilter(PriorityInfo)
}

func PriorityFilter(p Priority) Filter {
	return &levelFilter{
		p: p,
	}
}

type levelFilter struct {
	p Priority
}

func (l *levelFilter) Apply(next Writer) Writer {
	fw := &filteredWriter{
		p:    l.p,
		next: next,
	}

	for i := PriorityTrace; i <= PriorityAudit; i++ {
		fw.allow[i-1] = i >= l.p
	}
	return fw
}

// Filter is an output middleware
type Filter interface {
	Apply(Writer) Writer
}

type FilterFunc func(Writer) Writer

func (fn FilterFunc) Apply(wr Writer) Writer {
	return fn(wr)
}

// ApplyGlobalFilter pushes a filter onto the chain
// of filters being applied to the global output
func ApplyGlobalFilter(f Filter) {
	globalOutput = f.Apply(globalOutput)
}
