module bitbucket.org/dkolbly/logging

go 1.14

require (
	github.com/agnivade/levenshtein v1.1.0
	github.com/mattn/go-isatty v0.0.4
	golang.org/x/sys v0.0.0-20190124100055-b90733256f2e // indirect
)
