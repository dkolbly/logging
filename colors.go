package logging

import (
	"errors"
	"fmt"
	"strconv"
)

var colorReset = []byte("\033[0m")

func colorResetFrag(ctx *outputContext) {
	// bypass the ctx.Write() layer because we don't want this to
	// count against our columns
	ctx.dst.Write(colorReset)
}

func makeColorResetFrag(_ string) (fragmentFormatter, error) {
	return colorResetFrag, nil
}

var ErrTooManyColors = errors.New("too many colors mentioned")
var ErrNoColorSpecified = errors.New("no color specified")

func makeFixedColorFrag(options string) (fragmentFormatter, error) {
	colorseq, err := parseColor(options)
	if err != nil {
		return nil, err
	}
	return func(ctx *outputContext) {
		ctx.dst.Write(colorseq)
	}, nil
}

func levelColorFrag(ctx *outputContext) {
	c := colorPalette[ctx.Level()]
	if c != nil {
		ctx.dst.Write(c)
	}
}

func makeColorFrag(options string) (fragmentFormatter, error) {
	if options == "" {
		// special case for level-dependent coloring
		return levelColorFrag, nil
	}

	if options[0] != '=' {
		return nil, ErrNoColorSpecified
	}
	return makeFixedColorFrag(options[1:])
}

type ErrInvalidColorSpec struct {
	spec string
}

func (err ErrInvalidColorSpec) Error() string {
	return fmt.Sprintf("invalid color spec %q", err.spec)
}

func mustParseColor(spec string) []byte {
	seq, err := parseColor(spec)
	if err != nil {
		panic(err)
	}
	return seq
}

func parseColor(spec string) ([]byte, error) {
	bg := false
	faint := false
	bold := false

	choice := color(0)

	for _, ch := range spec {
		switch ch {
		case '_':
			faint = true
		case '*':
			bold = true
		case '#':
			bg = true
		case 'k':
			choice = colorBlack
		case 'r':
			choice = colorRed
		case 'g':
			choice = colorGreen
		case 'y':
			choice = colorYellow
		case 'b':
			choice = colorBlue
		case 'm':
			choice = colorMagenta
		case 'c':
			choice = colorCyan
		case 'w':
			choice = colorWhite
		case ' ':
		default:
			return nil, ErrInvalidColorSpec{spec}
		}
	}
	if choice == color(0) {
		return nil, ErrInvalidColorSpec{spec}
	}

	c := []byte{'\033', '['}

	if bg {
		choice += 10
	}
	c = strconv.AppendUint(c, uint64(choice), 10)
	if bold {
		c = append(c, ';', '1')
	}
	if faint {
		c = append(c, ';', '2')
	}
	c = append(c, 'm')
	return c, nil
}

type color int

const (
	colorBlack = (iota + 30)
	colorRed
	colorGreen
	colorYellow
	colorBlue
	colorMagenta
	colorCyan
	colorWhite
)

var colorPalette = map[Level][]byte{
	CRITICAL: mustParseColor("m*"),
	ERROR:    mustParseColor("r*"),
	WARNING:  mustParseColor("y*"),
	//NOTICE:   mustParseColor("g*"),
	INFO:  mustParseColor("g"),
	DEBUG: mustParseColor("w"),
}

func SetLogLevelColor(level Level, colorSpec string) error {
	c, err := parseColor(colorSpec)
	if err != nil {
		return err
	}
	colorPalette[level] = c
	return nil
}
