package logging

import (
	"sync"
)

var registryLock sync.Mutex
var registry = make(map[string]*moduleLogger)

func registerModule(m *moduleLogger) {
	registryLock.Lock()
	registry[m.name] = m
	registryLock.Unlock()
}

func (m *moduleLogger) Name() string {
	return m.name
}

func (m *moduleLogger) ApplyFilter(f Filter) {
	// fast filtering
	if lf, ok := f.(*levelFilter); ok {
		m.priority = lf.p
	} else {
		panic("??")
	}

	m.filter = f
}

type Module interface {
	Name() string
	ApplyFilter(Filter)
}

func FindModule(name string) Module {
	registryLock.Lock()
	defer registryLock.Unlock()
	m := registry[name]
	if m != nil {
		return m
	}
	return nil
}
