package logging

import (
	"context"
	"testing"
)

func TestDiversion(t *testing.T) {

	SetGlobalOutput(Null)

	ctx := context.Background()
	l := New("a")

	ctx, d1 := WithDiversion(ctx)
	l.Infof(ctx, "Hello")

	ctx, d2 := WithDiversion(ctx)
	l.Infof(ctx, "World")

	if len(d1.Entries) != 2 {
		t.Fatalf("Expected 2 entries in d1, got %d", len(d1.Entries))
	}
	if d1.Entries[0]["message"] != "Hello" {
		t.Fatalf("Expected first entry to be \"Hello\", got %q",
			d1.Entries[0]["message"])
	}
	if d1.Entries[1]["message"] != "World" {
		t.Fatalf("Expected second entry to be \"World\", got %q",
			d1.Entries[0]["message"])
	}

	if len(d2.Entries) != 1 {
		t.Fatalf("Expected 1 entries in d2, got %d", len(d2.Entries))
	}
	if d2.Entries[0]["message"] != "World" {
		t.Fatalf("Expected first entry to be \"World\", got %q",
			d2.Entries[0]["message"])
	}
}
