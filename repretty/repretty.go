// repretty is used to parse log messages and re-emit them in pretty form
package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path"
	"time"

	"bitbucket.org/dkolbly/logging"
	"github.com/mattn/go-isatty"
)

func isTerminal() bool {
	return isatty.IsTerminal(os.Stdout.Fd())
}

func main() {
	gcpMode := os.Getenv("GCP_MODE") == "1"

	indent := true
	color := isTerminal()

	logging.MustSetPrettyOutput(indent, color)
	logging.PrettyShowExtraFields()

	src := bufio.NewReader(os.Stdin)
	out := logging.GlobalOutput()

	for {
		line, err := src.ReadBytes('\n')
		if err != nil {
			if err == io.EOF {
				break
			}
			panic(err)
		}

		var fields map[string]interface{}
		err = json.Unmarshal(line, &fields)
		if err != nil {
			os.Stdout.Write(line)
			continue
		}
		if gcpMode {
			fields = fields["jsonPayload"].(map[string]interface{})
		}

		if !convertGoSlog(fields) {

			// handle the special case of time
			if t, ok := fields["@timestamp"]; ok {
				if tstr, ok := t.(string); ok {
					tm, err := time.Parse(time.RFC3339Nano, tstr)
					if err == nil {
						fields["@timestamp"] = tm
					} else {
						panic(err)
					}
				}
			} else if t, ok := fields["time"]; ok {
				if tstr, ok := t.(string); ok {
					tm, err := time.Parse(time.RFC3339Nano, tstr)
					if err == nil {
						fields["@timestamp"] = tm
					} else {
						panic(err)
					}
				}
			}
		}
		out.Write(fields)
	}
}

func convertGoSlog(fields map[string]interface{}) bool {
	msg, ok := fields["msg"]
	if !ok {
		return false
	}

	t, ok := fields["time"]
	if !ok {
		return false
	}

	tstr, ok := t.(string)
	if !ok {
		return false
	}

	tm, err := time.Parse(time.RFC3339Nano, tstr)
	if err == nil {
		fields["@timestamp"] = tm
	} else {
		panic(err)
	}

	fields["message"] = msg
	delete(fields, "msg")

	if convertSlogSource(fields) {
		delete(fields, "source")
	} else {
		panic("what")
	}

	return true
}

func convertSlogSource(fields map[string]interface{}) bool {
	jsource, ok := fields["source"]
	if !ok {
		return false
	}
	source, ok := jsource.(map[string]interface{})
	if !ok {
		return false
	}
	jfile, ok := source["file"]
	if !ok {
		return false
	}
	file, ok := jfile.(string)
	if !ok {
		return false
	}

	fields["loc_file"] = file
	file = path.Base(file)

	jline, ok := source["line"]
	if !ok {
		fields["loc"] = file
		return true
	}
	line, ok := jline.(float64)
	if !ok {
		fields["loc"] = file
		return true
	}
	fields["loc"] = fmt.Sprintf("%s:%d", file, int(line))
	return true
}
