package logging

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"sort"
	"strings"
)

type PrettyOutput struct {
	dest      io.Writer
	template  []fragmentFormatter
	showExtra bool
	inColor   bool
}

const indentTextFormat = "%{color}%{time:15:04:05.000} %{level:-8s} [%{module}|%{source:%s:%d}]%{/color} %{msgid}%{leftmargin}%{message}"
const noindentTextFormat = "%{color}%{time:15:04:05.000} %{level:-8s} [%{module}|%{source:%s:%d}]%{/color} %{msgid}%{message}"

// SetHumanOutput configures human-readable output if the output is a
// tty. If forceJSON is true, then the output will be JSON even if the
// output is a terminal.  If disableColor is true, then color will
// be disabled even for human-readable output.  The level parameter
// sets the default logging level, `"DEBUG"` to capture debug-level
// messages, `"INFO"` to only capture info and above messages, etc.
//
// This API is likely to change.
func SetHumanOutput(forceJSON, disableColor bool, level string) {
	if !forceJSON {
		tty := isTerminal(os.Stdout)
		if tty {
			color := !disableColor
			MustSetPrettyOutput(true, color)
		}
	}

	lvl := Level(strings.ToLower(level))
	if _, ok := levelToPriority[lvl]; ok {
		ApplyGlobalFilter(LevelFilter(lvl))
	}
}

func StdRedirect(level string, dest io.Writer) {
	if f, ok := dest.(*os.File); ok {
		if isTerminal(f) {
			MustSetPrettyOutput(true, true)
			globalOutput.(*PrettyOutput).dest = dest
			return
		}
	}
	globalOutput = &jsonOutput{dest}
}

// PrettyShowExtraFields configures human-readable output to include
// any additional fields that are not included in the formatted
// output
func PrettyShowExtraFields() {
	w := globalOutput
	for {
		if p, ok := w.(*PrettyOutput); ok {
			p.showExtra = true
			return
		}
		wrapped, ok := w.(Unwrapper)
		if !ok {
			return
		}
		w = wrapped.Unwrap()
	}
}

type Unwrapper interface {
	Unwrap() Writer
}

// MustSetPrettyOutput sets up the default global output to be the
// human-readable form to os.Stdout.  If indent is true, then
// multi-line messages are indented to line up with the message part
// of the log output line.  If color is true, the output is colorized.
//
// Unlike SetHumanOutput(), this function does not care what kind
// of output it is (i.e., it does not check for tty-ness)
//
// This API is likely to change.
func MustSetPrettyOutput(indent bool, color bool) {
	globalOutput = makePrettyOutput(indent, color, os.Stdout)
}

func makePrettyOutput(indent bool, color bool, next io.Writer) *PrettyOutput {
	pattern := noindentTextFormat
	if indent {
		pattern = indentTextFormat
	}
	colorfrags, nocolorfrags, _, err := compilePattern(pattern)
	if err != nil {
		panic(err)
	}
	po := &PrettyOutput{
		dest:    next,
		inColor: color,
	}
	if color {
		po.template = colorfrags
	} else {
		po.template = nocolorfrags
	}
	return po
}

func (po *PrettyOutput) Write(src map[string]interface{}) error {
	ctx := &outputContext{
		src:   src,
		color: po.inColor,
	}
	for _, frag := range po.template {
		frag(ctx)
	}

	if po.showExtra {
		appendExtra(&ctx.dst, po.inColor, src)
	}
	ctx.dst.WriteByte('\n')

	_, err := po.dest.Write(ctx.dst.Bytes())
	return err
}

type extraField struct {
	key   string
	value interface{}
}

type extrasList []extraField

func (l extrasList) Len() int           { return len(l) }
func (l extrasList) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }
func (l extrasList) Less(i, j int) bool { return l[i].key < l[j].key }

func appendExtra(dst *bytes.Buffer, inColor bool, src map[string]interface{}) bool {
	// collect the extras
	var tmp [10]extraField // avoid allocation for not very many
	lst := tmp[:0]
	for k, v := range src {
		if !intemplate[k] {
			lst = append(lst,
				extraField{
					key:   k,
					value: v,
				})
		}
	}
	if len(lst) == 0 {
		return false
	}
	if len(lst) > 1 {
		// sorting is necessary
		sort.Sort(extrasList(lst))
	}

	// print them
	for i, x := range lst {
		k := x.key
		v := x.value
		dst.WriteByte(' ')
		if i == 0 {
			if inColor {
				dst.Write(blue)
			}
		}
		tmp, err := json.Marshal(v)
		if err == nil {
			fmt.Fprintf(dst, "%s=%s", k, tmp)
		} else {
			fmt.Fprintf(dst, "%s=%v", k, v)
		}
	}
	if inColor {
		dst.Write(colorReset)
	}
	return true
}

var blue []byte
var msgidblue []byte

func init() {
	blue, _ = parseColor("b")
	msgidblue, _ = parseColor("b*")
}

// map of message object keys that are accounted for in the
// standard template (and hence we don't show them as part
// of appending extra fields to the end of the message)
var intemplate = map[string]bool{
	"time":       true,
	"module":     true,
	"level":      true,
	"@timestamp": true,
	"loc_file":   true,
	"loc":        true,
	"message":    true,
	"msgid":      true,
}
