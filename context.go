package logging

import (
	"context"
)

type Frame struct {
	key   interface{}
	value interface{}
	prev  *Frame
}

func F(key string, value interface{}) Frame {
	return Frame{
		key:   key,
		value: value,
	}
}

type contextkey int

const (
	logframes = contextkey(iota)
	keyTee
	keyOutput
)

func frames(c context.Context) *Frame {
	v := c.Value(logframes)
	if v == nil {
		return nil
	}
	return v.(*Frame)
}

func set(old context.Context, key, value interface{}) context.Context {
	return context.WithValue(
		old,
		logframes,
		&Frame{key, value, frames(old)})
}

func Set(old context.Context, key string, value interface{}) context.Context {
	return set(old, key, value)
}

func Setv(old context.Context, pairs ...interface{}) context.Context {
	f := frames(old)
	n := len(pairs) / 2
	for i := 0; i < n; i++ {
		f = &Frame{
			pairs[i*2].(string),
			pairs[i*2+1],
			f,
		}
	}
	return context.WithValue(old, logframes, f)
}

func Get(ctx context.Context, key string) interface{} {
	for f := frames(ctx); f != nil; f = f.prev {
		if f.key == key {
			return f.value
		}
	}
	return nil
}

// Reset copies the frame chain from the old context to a new
// context, returning an extended version of the *new* context.
func Reset(new, old context.Context) context.Context {
	return context.WithValue(new, logframes, frames(old))
}

func ContextOutput(ctx context.Context) Writer {
	for f := frames(ctx); f != nil; f = f.prev {
		if f.key == keyOutput {
			return f.value.(Writer)
		}
	}
	return nil
}
