// textpipe provides a plain-text writer adapter, so that
// we can be configured as a stdlog logger as by log.New
//
//	import "log"
//
//	log.New(textpipe.To(place), "foo", textpipe.LogFlags)

package textpipe

import (
	"bytes"
	"log"
	"path"
	"regexp"
	"time"

	"bitbucket.org/dkolbly/logging"
)

const LogFlags = log.Ldate | log.Ltime | log.Lmicroseconds | log.LUTC | log.Llongfile

func New(name string) *log.Logger {
	return log.New(To(logging.GlobalOutput()), name, LogFlags)
}

func To(w logging.Writer) *Writer {
	return &Writer{
		output: w,
	}
}

type Writer struct {
	output logging.Writer
	buffer []byte
}

// 1               2              3          4        5
var plain = regexp.MustCompile(`^([0-9/]+ [0-9:]+(\.[0-9]+)?) \[([A-Z]+)\] ([^:]+): (.*)$`)

// 1               2            3       4         5          6        7
var full = regexp.MustCompile(`^([0-9/]+ [0-9:]+(\.[0-9]+)?) (/[^:]+)(:\d+): \[([A-Z]+)\] ([^:]+): (.*)$`)

// example:

// [Foo]2019/03/31 18:21:59.937458 /u/donovan/go/src/bitbucket.org/dkolbly/foo/foo.go:44: Help
//
//	<-------1------>  <-------2-------<----3--->-> <--4---><-5-->  <6->
var mprefix = regexp.MustCompile(`^\[([A-Za-z0-9_.]+)\]([0-9/]+ [0-9:]+(\.[0-9]+)?) (/[^:]+)(:\d+): (.*)$`)

func (w *Writer) Write(buf []byte) (int, error) {
	w.buffer = append(w.buffer, buf...)

	for {
		k := bytes.IndexByte(w.buffer, '\n')
		if k < 0 {
			break
		}
		line := w.buffer[:k]
		if m := full.FindSubmatch(line); m != nil {
			w.parsefull(line, m)
		} else if m := plain.FindSubmatch(line); m != nil {
			w.parseplain(line, m)
		} else if m := mprefix.FindSubmatch(line); m != nil {
			w.parsemoduleprefix(line, m)
		} else {
			/* ignore it */
		}
		w.buffer = w.buffer[k+1:]
	}
	return len(buf), nil
}

func (w *Writer) parsefull(line []byte, m [][]byte) {

	t, err := parsetime(m[1], m[2])
	if err != nil {
		panic(err)
		return
	}
	x := map[string]interface{}{
		"@timestamp": t,
		"level":      downlevel(string(m[5])),
		"module":     string(m[6]),
		"message":    string(m[7]),
		"loc":        path.Base(string(m[3])) + string(m[4]),
	}
	w.output.Write(x)
}

func (w *Writer) parseplain(line []byte, m [][]byte) {
	t, err := parsetime(m[1], m[2])
	if err != nil {
		panic(err)
		return
	}
	x := map[string]interface{}{
		"@timestamp": t,
		"level":      downlevel(string(m[3])),
		"module":     string(m[4]),
		"message":    string(m[5]),
	}
	w.output.Write(x)
}

func (w *Writer) parsemoduleprefix(line []byte, m [][]byte) {
	t, err := parsetime(m[2], m[3])
	if err != nil {
		panic(err)
		return
	}
	x := map[string]interface{}{
		"@timestamp": t,
		"level":      logging.INFO,
		"module":     string(m[1]),
		"message":    string(m[6]),
		"loc":        path.Base(string(m[4])) + string(m[5]),
	}
	w.output.Write(x)
}

func downlevel(up string) logging.Level {
	switch up {
	case "WARN":
		return logging.WARNING
	case "ERROR", "ERR":
		return logging.ERROR
	case "DEBUG":
		return logging.DEBUG
	case "INFO":
		return logging.INFO
	default:
		panic("//TODO LEVEL " + up)
	}
}

func parsetime(all, minor []byte) (time.Time, error) {
	if len(minor) == 0 {
		return time.Parse("2006/01/02 15:04:05", string(all))
	} else {
		return time.Parse("2006/01/02 15:04:05.999999", string(all))
	}
}
