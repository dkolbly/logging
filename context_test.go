package logging

import (
	"context"
	"testing"
)

func TestSetGet(t *testing.T) {
	ctx := context.Background()

	c := Set(Set(ctx, "foo", 123), "bar", "baz")

	x := Get(c, "foo")
	if x != 123 {
		t.Errorf("Expected 123, got %#v", x)
	}

	x = Get(c, "bar")
	if x != "baz" {
		t.Errorf("Expected \"baz\", got %#v", x)
	}

	x = Get(c, "baz")
	if x != nil {
		t.Errorf("Expected nil, got %#v", x)
	}
}

func TestSetv(t *testing.T) {
	ctx := context.Background()

	c := Setv(ctx, "foo", 123, "bar", "baz")

	x := Get(c, "foo")
	if x != 123 {
		t.Errorf("Expected 123, got %#v", x)
	}

	x = Get(c, "bar")
	if x != "baz" {
		t.Errorf("Expected \"baz\", got %#v", x)
	}

	x = Get(c, "baz")
	if x != nil {
		t.Errorf("Expected nil, got %#v", x)
	}
}
