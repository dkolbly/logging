package logging

import (
	"context"
	"fmt"
	"path"
	"runtime"
	"runtime/debug"
	"time"
)

type Logger interface {
	Infof(ctx context.Context, msg string, args ...interface{})
	Warningf(ctx context.Context, msg string, args ...interface{})
	Debugf(ctx context.Context, msg string, args ...interface{})
	Tracef(ctx context.Context, msg string, args ...interface{})
	Auditf(ctx context.Context, msg string, args ...interface{})
	Errorf(ctx context.Context, msg string, args ...interface{})
	Error(ctx context.Context, err error)
	ReportPanic(ctx context.Context, skip int, p interface{})
	Send(ctx context.Context, skip int, level Level, msg string, xtra ...Frame)
}

type moduleLogger struct {
	name     string
	priority Priority // fast filtering
	filter   Filter
}

func New(module string) Logger {
	l := &moduleLogger{
		name:     module,
		priority: 0,
	}
	registerModule(l)
	return l
}

func (l *moduleLogger) ReportPanic(ctx context.Context, skip int, p interface{}) {
	if l.priority > PriorityCritical {
		return
	}
	dump := string(debug.Stack())
	l.Send(ctx, skip+1, CRITICAL, fmt.Sprintf("panic: %s", p), Frame{"stack", dump, nil})
}

func (l *moduleLogger) Errorf(ctx context.Context, msg string, args ...interface{}) {
	if l.priority > PriorityError {
		return
	}
	formatted, xtra := layoutAndPopExtras(msg, args)
	l.Send(ctx, 1, ERROR, formatted, xtra...)
}

func (l *moduleLogger) Error(ctx context.Context, err error) {
	if l.priority > PriorityError {
		return
	}
	var xtra []Frame
	if mider, ok := err.(MessageIDer); ok {
		f := Frame{
			key:   "msgid",
			value: mider.MessageID(),
		}
		xtra = []Frame{f}
	}
	l.Send(ctx, 1, ERROR, err.Error(), xtra...)
}

func (l *moduleLogger) Infof(ctx context.Context, msg string, args ...interface{}) {
	if l.priority > PriorityInfo {
		return
	}
	formatted, xtra := layoutAndPopExtras(msg, args)
	l.Send(ctx, 1, INFO, formatted, xtra...)
}

func (l *moduleLogger) Warningf(ctx context.Context, msg string, args ...interface{}) {
	if l.priority > PriorityWarning {
		return
	}
	formatted, xtra := layoutAndPopExtras(msg, args)
	l.Send(ctx, 1, WARNING, formatted, xtra...)
}

func (l *moduleLogger) Debugf(ctx context.Context, msg string, args ...interface{}) {
	if l.priority > PriorityDebug {
		return
	}
	formatted, xtra := layoutAndPopExtras(msg, args)
	l.Send(ctx, 1, DEBUG, formatted, xtra...)
}

func (l *moduleLogger) Auditf(ctx context.Context, msg string, args ...interface{}) {
	if l.priority > PriorityAudit {
		return
	}
	formatted, xtra := layoutAndPopExtras(msg, args)
	l.Send(ctx, 1, AUDIT, formatted, xtra...)
}

func (l *moduleLogger) Tracef(ctx context.Context, msg string, args ...interface{}) {
	if l.priority > PriorityTrace {
		return
	}
	formatted, xtra := layoutAndPopExtras(msg, args)
	l.Send(ctx, 1, TRACE, formatted, xtra...)
}

func layoutAndPopExtras(msg string, args []interface{}) (formatted string, xtra []Frame) {
	n := len(args)
	for n > 0 {
		last := args[n-1]
		if f, ok := last.(Frame); ok {
			xtra = append(xtra, f)
			args = args[:n-1]
			n--
		} else {
			break
		}
	}
	msgid, msg := Split(msg)
	if msgid != "" {
		f := Frame{
			key:   "msgid",
			value: msgid,
		}
		xtra = append(xtra, f)
	}

	formatted = fmt.Sprintf(msg, args...)
	return
}

func (l *moduleLogger) Send(ctx context.Context, skip int, level Level, msg string, xtra ...Frame) {

	fields := make(map[string]interface{})
	t := time.Now()
	fields["@timestamp"] = t
	fields["level"] = string(level)
	fields["message"] = msg

	fields["module"] = l.name

	var out Writer
	var tees []Writer
	for f := frames(ctx); f != nil; f = f.prev {
		if f.key == keyTee {
			tees = append(tees, f.value.(Writer))
		} else if f.key == keyOutput {
			if out == nil {
				out = f.value.(Writer)
			}
		} else if keystr, ok := f.key.(string); ok {
			if _, ok := fields[keystr]; !ok {
				fields[keystr] = f.value
			}
		}
	}
	if out == nil {
		out = globalOutput
	}
	for _, f := range xtra {
		if keystr, ok := f.key.(string); ok {
			fields[keystr] = f.value
		}
	}

	if _, ok := fields["loc"]; !ok {
		_, file, line, ok := runtime.Caller(skip + 1)
		if ok {
			fields["loc"] = fmt.Sprintf("%s:%d", path.Base(file), line)
			fields["loc_file"] = file
		}
	}

	if l.filter != nil {
		out = l.filter.Apply(out)
	}
	out.Write(fields)

	for _, t := range tees {
		if l.filter != nil {
			t = l.filter.Apply(t)
		}
		t.Write(fields)
	}
}
