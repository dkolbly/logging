# logging [![GoDoc](https://godoc.org/bitbucket.org/dkolbly/logging?status.svg)](https://godoc.org/bitbucket.org/dkolbly/logging)

This is yet another logging package for Go.  It is somewhat opinionated
in an effort to produce logs messages that are both human and machine
readable.

## Context

The primary purpose of this particular package was to experiment with
the context-passing pattern for logging, having had a mild allergic
reaction to `rs/xlog` style where the logger itself is included in the
context and a severe allergic reaction to the use of side-effects to
add annotation fields to the logging context.  Hence, all the logging
functions here take a `context.Context` argument and can use the
current context to *annotate* the logging event.  The logger itself is
still defined by the logging instance, which is a module.

## Modules

Each logging instance (*i.e.*, as produced by `New()`) defines a logging
module.  By convention, modules follow Go package boundaries (*i.e.*,
each Go package defines its own logging instance.)

## Output

This package does include a global "backstop" logging output.  In
addition, within a given context, logging be diverted or "tee'd" to
other outputs (see `Tee()`)

By default, the output is in machine-readable JSON format (one line
per JSON object) with a structure compatible with Splunk.

Indented for easy readability:

``` javascript
{
  "@timestamp": "2018-04-04T06:16:07.948198432-05:00",
  "level": "debug",
  "loc": "hello.go:13",
  "loc_file": "/tmp/hello.go",
  "module": "hello",
  "message": "Test",
}
```

If pretty output is configured (either directly, or by using
SetHumanOutput() when stdout is a terminal), the data is more
consumable by people.

``` text
06:53:49.975 DEBUG    [hello|hello.go:15] Test
06:53:49.975 INFO     [hello|hello.go:19] Here is something else: 75
```

## Structure

Unlike earlier versions of logging I've worked with, this one models
the logging event as nothing more than a map.  This is important for
output writers (the `logging.Writer` interface defines a `Write`
method that simply takes such a map) but nobody else should much care
about this representation.

## Example

``` go
package main

import (
	"context"

	"bitbucket.org/dkolbly/logging"
)

var log = logging.New("hello")

func main() {
	ctx := context.Background()
	log.Debugf(ctx, "Test")
}
```

## Extra Annotations

Additional fields can be supplied by adding them to the context
using `Set()`.

``` go
package main

import (
	"context"

	"bitbucket.org/dkolbly/logging"
)

var log = logging.New("hello")

func main() {
	logging.SetHumanOutput(false, false, "DEBUG")
	logging.PrettyShowExtraFields()

	ctx := context.Background()
	log.Debugf(ctx, "Test")

	x := 75
	ctx = logging.Set(ctx, "other", x)
	log.Infof(ctx, "Here is another: %d", x)
}
```

That produces the following output in a terminal:

``` text
06:55:39.621 DEBUG    [hello|hello.go:16] Test
06:55:39.621 INFO     [hello|hello.go:20] Here is another: 75 other=75
```

Note that the extra field (`other=75`) is included in the human
readable output as a trailer on the message line.  This is enabled
with `ShowExtraFields()`.

Extra fields always appear in the JSON, and can override the standard
fields.
